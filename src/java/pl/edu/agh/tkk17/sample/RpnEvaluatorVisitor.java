package pl.edu.agh.tkk17.sample;

import java.util.Stack;

public class RpnEvaluatorVisitor implements NodeVisitor
{
    private Stack<Double> stack;

    public RpnEvaluatorVisitor()
    {
        this.stack = new Stack<Double>();
    }

    public Double getValue()
    {
        return this.stack.peek();
    }

    public void visit(NodeAdd node)
    {
        node.getLeft().accept(this);
        node.getRight().accept(this);
        Double a = this.stack.pop();
        Double b = this.stack.pop();
        Double c = b + a;
        this.stack.push(c);
    }

    public void visit(NodeMul node)
    {
        node.getLeft().accept(this);
        node.getRight().accept(this);
        Double a = this.stack.pop();
        Double b = this.stack.pop();
        Double c = b * a;
        this.stack.push(c);
    }

    public void visit(NodeSub node)
    {
        node.getLeft().accept(this);
        node.getRight().accept(this);
        Double a = this.stack.pop();
        Double b = this.stack.pop();
        Double c = b - a;
        this.stack.push(c);
    }

    public void visit(NodeDiv node)
    {
        node.getLeft().accept(this);
        node.getRight().accept(this);
        Double a = this.stack.pop();
        if(a == 0) {
            throw new OutputableException("Division by 0.");
        }
        Double b = this.stack.pop();
        Double c = b / a;
        this.stack.push(c);
    }

    public void visit(NodeNumber node)
    {
        String value = node.getValue();
        Double numericValue = Double.parseDouble(value);
        this.stack.push(numericValue);
    }
}
